const { Sequelize, Model, DataTypes, STRING } = require('sequelize');
const uuid  = require('uuid');
const unirest = require('unirest');
const sequelize = require('../../dbConfig').sequelize;

const Nationality = sequelize.define('Nationality',{
    id : {
        type : uuid,
        primaryKey : true,
        allowNull : false
    },
    nationality : {
        type : STRING,
        unique : true,
        allowNull : false
    }
});

Nationality === sequelize.models.Nationality;
module.exports = Nationality;