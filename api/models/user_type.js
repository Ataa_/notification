const { Sequelize, Model, DataTypes, STRING } = require('sequelize');
const uuid  = require('uuid');
const unirest = require('unirest');
const sequelize = require('../../dbConfig').sequelize;

const User_type = sequelize.define('user_type',{
    id : {
        type : uuid,
        primaryKey : true,
        allowNull : false
    },
    type : {
        type : STRING,
        unique : true,
        allowNull : false
    }
});

User_type === sequelize.models.User_type;
module.exports = User_type;
/*--create table user_(id varchar(50) primary key, user_type varchar(50) foreign key references user_type(id) ,email varchar(50) unique not null ,
--password_ varchar(50), state_ bit default 0, first_name varchar(50), middle_name varchar(50), last_name varchar(50), DoB varchar(50) ,
--gender varchar(10) check (gender in('female','male')) , ins_company varchar(50) , ins_policy_no varchar(50) not null , nationality_id varchar(50) foreign key references nationality(id),
--creation_user_id varchar(50) foreign key references user_(id*/