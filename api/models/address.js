const { Sequelize, Model, DataTypes, STRING } = require('sequelize');
const uuid  = require('uuid');
const unirest = require('unirest');
require('dotenv').config()
const sequelize = require('../../dbConfig').sequelize;

const Address = sequelize.define('Address',{
    Address_id :{
        type : uuid,
        primaryKey:true,
        allowNull : false
    },
    Country : {
        type : STRING,
        allowNull : false
    },
    City : {
        type : STRING,
        allowNull : false
    },
    Street : {
        type : STRING,
        allowNull : true
    },
    zipCode :{
        type : STRING,
        allowNull : false
    },
    gmLocation : {
        type : STRING,
        allowNull : true
    }
});

Address === sequelize.models.Address;
// module.exports= dbconnection();
module.exports=Address;