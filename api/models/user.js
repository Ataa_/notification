const { Sequelize, Model, DataTypes, STRING, DATE } = require('sequelize');
const uuid  = require('uuid');
const unirest = require('unirest');
const sequelize = require('../../dbConfig').sequelize;
const user_type = require('./user_type');
const moment = require('moment')


const user_ = sequelize.define('user_',{
    id : {
        type : uuid,
        primaryKey : true,
        allowNull : false
    },
    user_type : {
        type : STRING,
        references : 'user_type',
        referencesKey : 'id'
    },
    email : {
        type : STRING,
        unique : true,
        allowNull : false
    },
    password_ : {
        type : STRING,
        allowNull : false
    },
    state_ : {
        type : Number,
    },
    first_name : {
        type : STRING,
        allowNull: false
    },
    middle_name : {
        type : STRING,
        allowNull: false
    },
    last_name : {
        type : STRING,
        allowNull: false
    },
    DoB : {
        type : DATE
    },
    gender :{
        type :STRING
    },
    ins_company : {
        type :STRING
    },
    ins_policy_no: {
        type :STRING
    },
    nationality_id : {
        type : STRING,
        references : 'nationality',
        referencesKey : 'id'
    },
    creation_user_id : {
        type : STRING,
        references : 'user_',
        referencesKey : 'id',
        defaultValue: null
    }
});

user_ === sequelize.models.user_;
module.exports = user_;




