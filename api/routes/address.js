const Address = require('../models/address');
const express = require('express');
const Router = express.Router();
const Sequelize = require('sequelize');
const uuid = require('uuid').v4;

function getAddressById(id){
    Address.findByPk(id)
    .then((address)=>{
        if(address){
            console.log('here1',address)
            return address
        }
        else{
            console.log('here',address)
            return null;  
        }
    });
}
Router.get('/',(req,res,next)=>{
    console.log('here');
    Address.findAll()
    .then((addresses)=>{
        res.status(200).json({
            "count" : addresses.length,
            "addresses" : addresses
        });
    }).catch((error)=>next(error));
});

Router.get('/:address_id',(req,res,next)=>{
    Address.findByPk(req.params.address_id)
    .then((address)=>{
        if(address){
            res.status(200).json({
                address : address
            });
        }else{
            res.status(404).json({
                message : "The required address does not exist."
            });
        }
    }).catch((error)=>{
        next(error);
    })
})
Router.post('/',(req,res,next)=>{
    console.log('here',uuid());
    const address = new Address(req.body);
    address.Address_id = uuid();
    console.log(address)
    address.save()
    .then((result)=>{
        console.log('address created');
        res.status(201).json({
            message : result
        });
    }).catch((error)=>{
        console.log('error');
        next(error);
    });
    
});
Router.patch('/:address_id',(req,res,next)=>{
    Address.update(req.body,{
        where : {'address_id':req.params.address_id}
    })
    .then((result)=>{
        console.log(req.params.address_id)
        var updated = getAddressById(req.params.address_id);
        console.log(updated)
        res.status(200).json({
            result : result,
            address : updated
        });
    }).catch((error)=>{
        console.log(error);
        next(error)
    })
});
Router.delete('/:address_id',(req,res,next)=>{
    
    Address.destroy({
        where :{ 'address_id':req.params.address_id}
    })
    .then((result)=>{
        res.status(200).json({
            "message" : result
        });
    }).catch((error)=>next(error));
})


module.exports = Router;