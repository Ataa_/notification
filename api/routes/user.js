const user_ = require('../models/user');
const user_type = require('../models/user_type');
const express = require('express');
const Router = express.Router();
const sequelize = require('../../dbConfig').sequelize;
const { Date, DateTime } = require('mssql');
const uuid = require('uuid').v4;
const moment = require('moment');
function getnationalityById(id){
    user_.findByPk(id)
    .then((user)=>{
        if(user){
            console.log('here1',user)
            return user
        }
        else{
            console.log('here',user)
            return null;  
        }
    });
}
Router.get('/',(req,res,next)=>{
    console.log('here11');
    user_.findAll()
    .then((users)=>{
        res.status(200).json({
            "count" : users.length,
            "users" : users
        });
    }).catch((error)=>next(error));
});

Router.get('/:user_id',(req,res,next)=>{
    user_.findByPk(req.params.user_id)
    .then((user)=>{
        if(user){
            res.status(200).json({
                user : user
            });
        }else{
            res.status(404).json({
                message : "The required user does not exist."
            });
        }
    }).catch((error)=>{
        next(error);
    })
})
async function client_type(type1){
    console.log('im heeereeee')
    const results =  await sequelize.query("select id from user_type where type_='"+type1+"'")
    console.log('wow',results)
    return result;
} 
Router.post('/',async (req,res,next)=>{
    let type= req.body.user_type; 
    let nationality = req.body.nationality;
    var typeID =  await sequelize.query("select id from user_type where type_='"+type+"'")
    var nationality_id =  await sequelize.query("select id from nationality where nationality='"+nationality+"'")
    var user = new user_(req.body); 
    user.id = uuid();
    user.user_type = typeID[0][0].id;
    user.nationality_id=nationality_id[0][0].id;
    var dob = req.body.DoB;
    dob = moment().format(dob);
    console.log(dob)
    user.DoB = dob;
    user.save()
    .then((result)=>{
        console.log('user created');
        res.status(201).json({
            message : result
        });
    }).catch((error)=>{
        console.log('error');
        next(error);
    });
    
});
Router.patch('/:user_id',(req,res,next)=>{
    user_.update(req.body,{
        where : {'id':req.params.user_id}
    })
    .then((result)=>{
        console.log(req.params.user_id)
        var updated = getuserById(req.params.user_id);
        console.log(updated)
        res.status(200).json({
            result : result,
            user : updated
        });
    }).catch((error)=>{
        console.log(error);
        next(error)
    })
});
Router.delete('/:user_id',(req,res,next)=>{
    user_.destroy({
        where :{ 'id':req.params.user_id}
    })
    .then((result)=>{
        res.status(200).json({
            "message" : result
        });
    }).catch((error)=>next(error));
})


module.exports = Router;