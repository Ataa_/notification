const Nationality = require('../models/nationality');
const express = require('express');
const Router = express.Router();
const Sequelize = require('sequelize');
const uuid = require('uuid').v4;

function getnationalityById(id){
    Nationality.findByPk(id)
    .then((nationality)=>{
        if(nationality){
            console.log('here1',nationality)
            return nationality
        }
        else{
            console.log('here',nationality)
            return null;  
        }
    });
}
Router.get('/',(req,res,next)=>{
    console.log('here');
    Nationality.findAll()
    .then((nationalities)=>{
        res.status(200).json({
            "count" : nationalities.length,
            "nationalities" : nationalities
        });
    }).catch((error)=>next(error));
});

Router.get('/:nationality_id',(req,res,next)=>{
    Nationality.findByPk(req.params.nationality_id)
    .then((nationality)=>{
        if(nationality){
            res.status(200).json({
                nationality : nationality
            });
        }else{
            res.status(404).json({
                message : "The required nationality does not exist."
            });
        }
    }).catch((error)=>{
        next(error);
    })
})
Router.post('/',(req,res,next)=>{//make sure that there's no nationality with the same name
    console.log('here',uuid());
    const nationality = new Nationality(req.body);
    nationality.id = uuid();
    console.log(nationality)
    nationality.save()
    .then((result)=>{
        console.log('nationality created');
        res.status(201).json({
            message : result
        });
    }).catch((error)=>{
        console.log('error');
        next(error);
    });
    
});
Router.patch('/:nationality_id',(req,res,next)=>{
    Nationality.update(req.body,{
        where : {'id':req.params.nationality_id}
    })
    .then((result)=>{
        console.log(req.params.nationality_id)
        var updated = getnationalityById(req.params.nationality_id);
        console.log(updated)
        res.status(200).json({
            result : result,
            nationality : updated
        });
    }).catch((error)=>{
        console.log(error);
        next(error)
    })
});
Router.delete('/:nationality_id',(req,res,next)=>{
    
    Nationality.destroy({
        where :{ 'id':req.params.nationality_id}
    })
    .then((result)=>{
        res.status(200).json({
            "message" : result
        });
    }).catch((error)=>next(error));
})


module.exports = Router;