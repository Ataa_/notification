const http = require('http');
const express = require('express');
const app = require('./app');


const server = http.createServer(app);
server.on('listening',()=>{
    console.log('ok, server is running');
});
server.listen(3003,()=>{
    console.log('connected to server on port '+3003);
});
