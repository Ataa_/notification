//this is a practice example
//created to play around with sequelize and mssql and api

const http = require('http');
const express = require('express');
const app = express();

const DBConnection = require('./dbConfig');
const env  = require('process');
const morgan = require('morgan');
const bp = require('body-parser');

const addressRoutes = require('./api/routes/address')
const nationalityRoutes = require('./api/routes/nationality');
const userRoutes = require('./api/routes/user');

require('dotenv').config()

DBConnection.dbconnection();



//using bp to parse incoming requests
app.use(bp.urlencoded({ extended: false }));
app.use(bp.json());
//using morgan as the logger
app.use(morgan('dev'));

app.use('/api/addresses',addressRoutes);
app.use('/api/nationalities',nationalityRoutes);
app.use('/api/users',userRoutes);

app.use((req,res,next)=>{
    console.log('in app.jss')
    res.status(404).json({
        message : 'the requested router was not found'
    })
});

module.exports = app;